module.exports = {
	variable: 'value'
}

breakpointVars = {
	palmEnd: 650,
	get tabStart(){
		return this.palmEnd + 1;
	},
	tabEnd: 1100,
	get deskStart(){
		return this.tabEnd + 1;
	}
}

colorVars = {
	colorTypeDark: 'rgb(60,60,60)',
	colorTypeLight: 'rgb(240,240,240)',
	colorPrimary: '#1C7858',
	colorSecondary: '#2A4874',
	colorGrey1: 'rgb(230,230,230)',
  colorGrey2: 'rgb(160,160,160)',
  colorGrey3: 'rgb(80,80,80)'
}

typographyVars = {
	baseFont: `"Overpass", "Helvetica Neue", sans-serif`,
  headerFont: `"Overpass", "Helvetica Neue", sans-serif`,
  base1: 1.7,
  ratio1: 1.25,
  base2: 1.9,
  ratio2: 1.25,
}

uiVars = {
	uiBorderRadius: '3px',
	uiFancyEasing: `cubic-bezier(.65,.05,.35,1)`
}


module.exports = Object.assign(
	{},
	breakpointVars,
	colorVars,
	typographyVars,
	uiVars
)