require 'sinatra/base'
require 'sinatra/json'
require 'sinatra/namespace'
require 'sinatra/reloader'
require './lib/amodule.rb'
require './lib/app_logger.rb'

class App < Sinatra::Base
	register Sinatra::Namespace

	enable :logging
	set :logging, AppLogger.logger

	configure :development do 
		register Sinatra::Reloader
		also_reload './lib/**/*.rb'
	end

	get '/' do 
		File.read(File.join('public', 'index.html'))
	end

	namespace '/api' do 
		get '/foo' do
			logger.info 'Hello'
			json bar: AModule.foo
		end
	end

	not_found do 
		File.read(File.join('public', '404.html'))
	end
end