require 'logger'

class AppLogger
  extend SingleForwardable

  def_delegators :logger, :info, :error, :warn, :level

  class << self
    def logger
      return @_logger if @_logger

      @_logger = Logger.new STDOUT
      @_logger.level = Logger::INFO
    end

    def suppress_logging
      logger.level = Logger::FATAL
    end
  end
end