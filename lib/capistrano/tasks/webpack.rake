after :'npm:install', :'deploy:webpack'

namespace :deploy do 
	desc 'build with webpack' 
	task :webpack do 
		on roles(:all) do 
			within release_path do
				execute :npm, 'run build'
			end
		end
	end
end