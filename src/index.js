import styles from './styles/style.css';

import Vue from 'vue';
import vueInfiniteScroll from 'vue-infinite-scroll';
import {VueMasonryPlugin} from 'vue-masonry';

import axios from 'axios';

// https://masonry.desandro.com/
// https://infinite-scroll.com/api.html
// https://unsplash.com/developers
// https://picsum.photos/


////////// img getter helper

let imgRatios = [
	{w: 600, h: 400},
	{w: 400, h: 600},
	{w: 500, h: 400},
	{w: 400, h: 500},
	{w: 900, h: 600},
	{w: 600, h: 900},
	{w: 500, h: 500},
	{w: 640, h: 360},
	{w: 360, h: 640}
];

let baconLabels = [
	"Bacon ipsum dolor amet landjaeger swine.", 
	"Hamburger tenderloin burgdoggen picanha flank.", 
	"Ball tip beef porchetta ribeye sirloin.", 
	"Short ribs corned beef strip.", 
	"Pork belly bacon turducken venison."
]


function getImage() {
	let seed = Math.random().toString(36).substring(2, 10);
	let ratio = imgRatios[imgRatios.length * Math.random() | 0];
	let randParam = Math.floor(Math.random() * 10);
	let baconLabel = baconLabels[baconLabels.length * Math.random() | 0];
	
	return {
		src:`https://picsum.photos/seed/${seed}/${ratio.w}/${ratio.h}?random=${randParam}`,
		label: baconLabel
	}
}

////////// VUE tom foolery

Vue.use(VueMasonryPlugin);

let app = new Vue({
	el: '#app',
	data: {
		message: 'WHAT UP BITCHES',
		imgArray: []
	},
	directives: {
		vueInfiniteScroll
	},
	created() {
		for (var i = 0; i < 10; i++) {
			this.imgArray.push(getImage());	
		}
	},
	methods: {
    loadMore: function() {
      this.busy = true;

      setTimeout(() => {
        for (var i = 0, j = 10; i < j; i++) {
          this.imgArray.push(getImage());
        }
        this.busy = false;
      }, 1000);
    }
  }
});



////////// burger menu nav tray
let navTriggers = document.getElementsByClassName('navtrigger');

Array.from(navTriggers).forEach(function(n) {
	n.addEventListener('click', function(e) {
		toggleClass(document.getElementById('navtray'), 'navtray--open', "menutrigger");
		toggleClass(n, 'navtrigger--open');
	});
});

// toggle a class on an element
function toggleClass(element, className, gaCategory) {
	if (element.classList.contains(className)){
		element.classList.remove(className);
	} else {
		element.classList.add(className);
	}
}
